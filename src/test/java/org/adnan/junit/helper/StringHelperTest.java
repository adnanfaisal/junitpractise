package org.adnan.junit.helper;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringHelperTest {

	StringHelper stringHelper = new StringHelper();	
	
	@Test
	public void testTruncateAInFirst2Positions_startsWithTwoA() {
		assertEquals("CD",stringHelper.truncateAInFirst2Positions("AACD"));			
	}

	@Test
	public void testTruncateAInFirst2Positions_startsWithOneA() {	
		assertEquals("CD",stringHelper.truncateAInFirst2Positions("ACD"));				
	}
	
	@Test
	public void testTruncateAInFirst2Positions_noAatTheBeginning() {		
		assertEquals("CDF",stringHelper.truncateAInFirst2Positions("CDF"));				
	}
	
	@Test
	public void testTruncateAInFirst2Positions_AatTheEnd() {		
		assertEquals("CDFAA",stringHelper.truncateAInFirst2Positions("CDFAA"));				
	}
	
}
