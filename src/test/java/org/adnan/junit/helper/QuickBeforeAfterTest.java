package org.adnan.junit.helper;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author adnan
 *
 */
public class QuickBeforeAfterTest {

	public QuickBeforeAfterTest(){
		System.out.println("Inside constructor");		
	}
	
	@BeforeClass
	public static void classSetup(){ // before class must be static
		System.out.println("Before Class");
	}
	
	@Before
	public void setup(){
		System.out.println("Set up");
	}
	
	@Test
	public void test1() { // all test cases must be "public void"
		System.out.println("Inside test1");
	}

	@Test
	public void test2() {
		System.out.println("Inside test2");
	}
	
	@After
	public void tearDown(){
		System.out.println("Tear down");
	}
	
	@AfterClass
	public static void classTearDown(){ // after class must be static
		System.out.println("After Class");
	}
}
