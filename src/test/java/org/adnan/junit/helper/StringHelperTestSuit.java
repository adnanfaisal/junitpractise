package org.adnan.junit.helper;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ StringHelper_FirstLastCharacterSame_ParameterizedTest.class,
		StringHelper_Truncation_ParameterizedTest.class, StringHelperTest.class })
public class StringHelperTestSuit {

}
