package org.adnan.junit.helper;
/**
 * This test class demonstrates the 4 steps needed to use parameterized test.
 * Note that in a single test class, you can't write parameterized test for more
 * than one method because the compile will be confused about which constructor
 * to use.
 * 
 * @author Adnan Faisal
 * 
 */
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

// 1: RunWith
@RunWith(Parameterized.class)
public class StringHelper_Truncation_ParameterizedTest {

	private StringHelper stringHelper = new StringHelper();	
	private String input;
	private String expectedOutput;
	
	// 3: a constructor with the input and expected output as parameters
	public StringHelper_Truncation_ParameterizedTest(String input, String expectedOutput){
		this.input = input;
		this.expectedOutput = expectedOutput;
	}
	
	
	//2. A @Parameters annotated method that returns a collection of parameters
	@Parameters
	public static Collection<String[]> testConditions(){
		String[][] parameters = {{"AACD", "CD"}, 
				{"ACD", "CD"}, 
				{"CDF", "CDF"},
				{"CDFAA", "CDFAA"}};
		
		return Arrays.asList(parameters);
	}
	
	//4. Update the parameters with input and expectedOutput variables
	@Test
	public void testTruncateAInFirst2Positions() {
		assertEquals(expectedOutput,stringHelper.truncateAInFirst2Positions(input));			
	}


	
}
