package org.adnan.junit.helper;
/**
 * This test class demonstrates the 4 steps needed to use parameterized test.
 * Note that in a single test class, you can't write parameterized test for more
 * than one method because the compile will be confused about which constructor
 * to use.
 * 
 * @author Adnan Faisal
 * 
 */
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

// 1: RunWith
@RunWith(Parameterized.class)
public class StringHelper_FirstLastCharacterSame_ParameterizedTest {

	private StringHelper stringHelper = new StringHelper();	
	private String input;
	private boolean expectedOutput;
	
	// 3: a constructor with the input and expected output as parameters
	public StringHelper_FirstLastCharacterSame_ParameterizedTest(String input, boolean expectedOutput){
		this.input = input;
		this.expectedOutput = expectedOutput;
	}
	
	
	//2. A @Parameters annotated method that returns a collection of parameters
	@Parameters
	public static Collection<Object[]> testConditions(){
	
		// A possible implementation follows 
		/* Object[][] parameters = {{"AACDAA", true}, 
		/		{"AACDDAA", true}, 
		/		{"CCDFFF", false}};		
		/  return Arrays.asList(parameters);
		 */
		
		//Alternative 
		return Arrays.asList( new Object[][]{{"AACDAA", true}, 
						{"AACDDAA", true}, 
						{"CCDFFF", false}}					
				);
	}
	
	//4. Update the parameters with input and expectedOutput variables
	@Test
	public void testAreFirstAndLastTwoCharactersTheSame_BasicPositiveAndNegative() {
		assertEquals(expectedOutput,stringHelper.areFirstAndLastTwoCharactersTheSame(input));			
	}


	
}
