package org.adnan.junit.helper;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;

public class ArrayTest {

	@Test
	public void testArrayEquals_BasicCase() {
		int[] given = {10, 11, 3, 7};
		int[] sorted = {3, 7, 10, 11};
		
		Arrays.sort(given);
		
		assertArrayEquals(sorted,given);		
	//	assertEquals(sorted,given); // will return false as it will compare if the two objects are the same				
	}
	
	@Test(expected=NullPointerException.class)
	public void testArrayEquals_NullPointerException() {
		int[] given = null;		
		Arrays.sort(given);				
	}
	
	
	/**
	 * This method should complete execution in less than some ms
	 */	
	@Test(timeout=500) 
	public void testArraysEquals_ProcessingTimeLessThan10ms(){
		int[] given = {10, 11, 3, 7};
		for(int i=0;i<1_000_000;i++){
			Arrays.sort(given);
			for(int j=0;j<given.length;j++)
				given[j] = Integer.MAX_VALUE - j;		
		}		
	}
	

}
